import { StatusBar } from 'expo-status-bar';
import React from 'react';

import { StyleSheet, Text, View, Button, TextInput } from 'react-native';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { NavigationContainer } from '@react-navigation/native';
import TestleiterScreen from './screens/TestLeiterScreen';

const Drawer = createDrawerNavigator();

function Tester({ navigation }) {
  return (
    <View style={styles.container}>
      <Button onPress={() => navigation.goBack()} title="Go back home" />
    </View>
  );
}


export default function App() {
  return (
    <NavigationContainer>
      <Drawer.Navigator initialRouteName="TesterScreen">
        <Drawer.Screen name="TestleiterScreen" component={TestleiterScreen} />
        <Drawer.Screen name="Tester" component={Tester} />
      </Drawer.Navigator>
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});