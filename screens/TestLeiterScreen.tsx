import React, { useState } from 'react';
import firestore from '../firebase';
import { StyleSheet, Text, View, Button, TextInput } from 'react-native';
import { useForm, Controller } from 'react-hook-form';
import { superstructResolver } from '@hookform/resolvers';

// import { is, struct, object, string } from 'superstruct';

type FormData = {
  firstName: string;
  lastName: string;
  email: string;
  resolver: any
};

/* const formSchema = struct({
  firstName: 'string',
  lastName: 'number',
  email: 'email'
}); */


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

const TestleiterScreen = ({ navigation }) => {
  const { control, handleSubmit, errors, formState } = useForm<FormData>();
  const firstNameInputRef = React.useRef();
  const lastNameInputRef = React.useRef();

  const [submitError, setSubmitError] = useState('');

  const onSubmit = (data: FormData) => persistData(data);

  const inputStyle = {
    height: 40,
    width: 300,
    paddingHorizontal: 5,
    marginBottom: 5,
    borderColor: 'gray',
    borderWidth: 1,
  };
  const persistData = async (data: FormData) => {
    try {
      let user = await firestore().collection('users').add(data);
      console.log(user.id);
    } catch (e) {
      setSubmitError(e);
    }
  };

  return (
    <View style={styles.container}>
      <View>
        <Text>First Name</Text>
        <Controller
          control={control}
          name="firstName"
          rules={{
            pattern: {
              value: /^[a-zA-Z_äÄöÖüÜß]+$/,
              message: 'Name ohne Nummer oder',
            },
          }}
          defaultValue=""
          render={({ onChange, onBlur, value, ...props }) => (
            <TextInput
              {...props}
              placeholder="Name"
              style={inputStyle}
              onBlur={onBlur}
              onChangeText={(value) => onChange(value)}
              value={value}
            />
          )}
        />
        {errors.firstName && <Text>{errors.firstName.message}.</Text>}
      </View>
      <Button title="Submit" onPress={handleSubmit(onSubmit)} />
    </View>
  );
};

export default TestleiterScreen;

