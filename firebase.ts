import * as firebase from 'firebase';
import 'firebase/firestore';

const firebaseConfig = {
  apiKey: 'AIzaSyCzwY1-bb5b60UJYacy31XdxfmtMrsnzMU',
  authDomain: 'neuss-sportsverband.firebaseapp.com',
  databaseURL: 'https://neuss-sportsverband.firebaseio.com',
  projectId: 'neuss-sportsverband',
  storageBucket: 'neuss-sportsverband.appspot.com',
  messagingSenderId: '812012952675',
  appId: '1:812012952675:web:bd9e1df1b71c759e0fac56',
  measurementId: 'G-W3DYTZPBY1',
};

firebase.initializeApp(firebaseConfig);

const db = firebase.firestore();
export default () => db;
